const express = require('express');

const PORT = 3000;
const server = express();

server.use('/', (req, res) => {
  res.send('Main Page RythmeOK');
});

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});